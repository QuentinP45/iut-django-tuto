from django.db import models

# Create your models here.

class Task(models.Model):
    name = models.CharField(max_length=250)
    description = models.TextField()
    created_date = models.DateField(auto_now=True)
    schedule_date = models.DateField(null=True)
    due_date = models.DateField(null=True)
    closed = models.models.BooleanField(default=false)

    def __str__(self):
        return self.name
